import React from "react";
import PokemonCard from "../components/PokemonCard";
import { Query } from 'react-apollo'
import gql from 'graphql-tag'

const POKEMON_QUERY = gql`
    {
        pokemon {
            id
            name 
            typing {
                name
            }
        }
  }
`

class PokemonContainer extends React.Component {
    render() {
        return (
            <Query query={POKEMON_QUERY}>
                {({ loading, error, data }) => {
                    if (loading) return <div>Fetching</div>
                    if (error) return <div>{console.log(error)}</div>
    
                    const pokemonToRender = data.pokemon
    
                    return (
                        pokemonToRender.map(poke => {
                            return (
                                <PokemonCard 
                                    key={poke.id} pokemon={poke}
                                />
                            )
                        })  
                    )
                }}
            </Query>
        )
    }
}

export default PokemonContainer