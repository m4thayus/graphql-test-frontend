import React from "react"

function PokemonCard(props){
    let {pokemon} = props

    return (
        <div>
            <h2>{pokemon.name}</h2>
            <ul>
                {pokemon.typing.map(type => <li key={type.name}>{type.name}</li>)}
            </ul>
        </div> 
    )
}

export default PokemonCard