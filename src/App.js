import React from 'react';
import './App.css';
import PokemonContainer from './containers/PokemonContainer';


function App() {
    return (
        <React.Fragment>
            <PokemonContainer  />
        </React.Fragment>       
    );
}

export default App;
